import json
import pymongo
from bson import json_util
from bson.objectid import ObjectId
class SocketMessage():

	def decodeMessage(self, inst, mess):
		parse_message = json.loads(mess)
		funct = getattr(inst, parse_message['method'])
		if callable(funct):
			funct(parse_message['message'])
		else:
			print 'invalid method'

	def encodeMessage(self, funct, mess):
		message = {}
		message['method'] = funct
		message['message'] = mess
		return json.dumps(message,default=json_util.default)