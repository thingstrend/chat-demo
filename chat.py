import logging
import tornado.auth
import tornado.escape
import tornado.ioloop
import tornado.web
import tornado.websocket
import os.path
import uuid
import json
import datetime
import pymongo
from bson import json_util
from bson.objectid import ObjectId
import tornado.httpserver
from tornado import gen
from tornado.options import define, options, parse_command_line
from SocketMessage import SocketMessage
define("port", default=8888, help="run on the given port", type=int)

settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : True
}

clients = {}
class WebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        self.name = ''
        self.coder = SocketMessage()
        print 'new connection'

    def introduction(self, mess):
        self.name = mess
        print "added client"
        clients[self.name] = self
        the_date = datetime.datetime.fromtimestamp(int("1284101485")).strftime('%Y-%m-%d %H:%M:%S')
        message = self.coder.encodeMessage('updateClients', '')
        server = self.coder.encodeMessage('chat', {'data':'Server at ' + the_date + ' : ' + self.name + ' join chat.'})
        for i in clients:
            clients[i].write_message(message)
            clients[i].write_message(server)
    def broadcast(self, mess):      
        the_date = datetime.datetime.fromtimestamp(int("1284101485")).strftime('%Y-%m-%d %H:%M:%S')
        message = self.coder.encodeMessage('chat', {'data':self.name +' at ' + the_date + ' : ' + mess, 'index': the_date})
        for i in clients:
            clients[i].write_message(message)

    def on_message(self, message):
        print json.loads(message)
        self.coder.decodeMessage(self, message)
 
    def on_close(self):
        try:
            clients.pop(self.name, None)
            message = self.coder.encodeMessage('updateClients', '')
            the_date = datetime.datetime.fromtimestamp(int("1284101485")).strftime('%Y-%m-%d %H:%M:%S')
            server = self.coder.encodeMessage('chat', {'data':'Server at ' + the_date + ' : ' + self.name + ' left chat.'})
            for i in clients:
                clients[i].write_message(message)
                clients[i].write_message(server)
        except ValueError:
            print 'client not added'
        print 'connection closed'


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

class ClientsHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Content-Type", "application/json")
        names = []
        for i in clients:
            names.append({'name': clients[i].name})
        self.write(json.dumps(list(names),default=json_util.default))

def main():
    parse_command_line()
    app = application = tornado.web.Application([
    	(r'/', IndexHandler),
    	(r'/index', IndexHandler),
        (r'/clients', ClientsHandler),
    	(r'/websocket', WebSocket),
	],**settings)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()