ws = new socket("ws://" + location.host + "/websocket", function(){
	var name = prompt("Enter name: ");
	ws.emit('introduction', name);
});
ws.logger(0);
ws.on('chat', function(mes){
	var messageController = angular.element(document.querySelector('#chatMessages_div')).scope();
	messageController.addItem(mes);
});

ws.on('updateClients', function(message){
	var chatController = angular.element(document.querySelector('#cl_list')).scope();
	chatController.update();
});


