function socket(url, options, onConnect){
	if((typeof options == 'undefined' || typeof options == 'function') && typeof options != 'object')
		this.socketHandle = new WebSocket(url);
	else
		this.socketHandle = new WebSocket(url, options);
	if(typeof options == 'function')
		onConnect = options;
	this.events = [];
	this.logLevel = 0;//0 no logs, 1 only errors, 2 all logs
	var self = this;
	this.socketHandle.onmessage = function(message){
		if(self.logLevel > 1)
			console.log('WebSocket Message ' + message.data);
		try
			{ 
				var p_data = self.parseMessage(message.data);
				return self.events[p_data.method](p_data.message);
			}
		catch(e)
			{
			   if(self.logLevel > 0)
					console.log('Error Parsing JSON And Loading Event' + e);
			}
	};
	this.socketHandle.onerror = function (error) {
		if(self.logLevel > 0)
			console.log('WebSocket Error ' + error);
		};
	this.socketHandle.onopen = function(){
		if(typeof onConnect == 'function')//call on connect function
			onConnect();
	}
}

socket.prototype.on = function(event, funct) {
	this.events[event] = funct;
};

socket.prototype.parseMessage = function(message){
	return JSON.parse(message);
};

socket.prototype.encodeMessage = function(message){
	return JSON.stringify(message);
};

socket.prototype.emit = function(functName, message){
	var data = {};
	data.method = functName;
	data.message = message;
	this.socketHandle.send(this.encodeMessage(data));
};

socket.prototype.logger = function(val) {
	this.logLevel = val;
};

socket.prototype.close = function() {
	this.socketHandle.onclose = {};
	this.socketHandle.close();
};