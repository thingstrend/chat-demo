var chatApp = angular.module('chatApp', ['chatAppServices']);

chatApp.controller('chatMessages', function($scope){
    $scope.messages = []
    $scope.addItem = function(message){
        $scope.messages.push(message);
        $scope.$apply();
    }
});

//define chat list controller
chatApp.controller('chat_form', function($scope){
    
    $scope.send = function(message){
       console.log(message);
       ws.emit('broadcast', message.data);
       document.querySelector("#message").value = '';
    }
});

chatApp.controller('clientsList', ['$scope', 'Clients', function($scope, Clients){
    $scope.clients = Clients.query()
    $scope.update = function(){
        $scope.clients = Clients.query()
    }
}]);
