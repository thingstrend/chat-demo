
ws = new WebSocket("ws://" + location.host + "/websocket");
ws.binaryType = 'blob'
ws.onopen = function(){
	var name = prompt("Enter name: ");
	chat.introduction(name);
	startStream();
}
ws.onmessage = function(message){
	console.log(message.data);
	var data = JSON.parse(message.data);
	if(data.method == 'chat'){
		var messageController = angular.element(document.querySelector('#chatMessages_div')).scope();
		messageController.addItem(data.message);
	}
	else if(data.method == 'updateClients'){
		var chatController = angular.element(document.querySelector('#cl_list')).scope();
		chatController.update();
	}
	
}
function chatInterface(){
	this.onopen = null
	this.onmessage = null
	this.introduction = function(name){
		var data = {};
		data.method = 'introduction';
		data.message = name;
		// console.log(JSON.stringify(data));
		ws.send(JSON.stringify(data));
	}
	this.sendMessage = function(message){
		var data = {};
		data.method = 'broadcast';
		data.message = message;
		ws.send(JSON.stringify(data));
	}
	
	this.getClients = function(){
		var data = {};
		data.method = 'clients';
		data.message = '';
		return JSON.parse(ws.send(JSON.stringify(data)));
	}
	
}
var chat = new chatInterface();

