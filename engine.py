import logging
import tornado.auth
import tornado.escape
import tornado.ioloop
import tornado.web
import tornado.websocket
import os.path
import uuid
import json
import pymongo
from bson import json_util
from bson.objectid import ObjectId
import tornado.httpserver
from tornado import gen
from tornado.options import define, options, parse_command_line
from SocketMessage import SocketMessage
define("port", default=8888, help="run on the given port", type=int)


settings = {
    #gameweb client files are configured below
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : True
}

class IndexHandler(tornado.web.RequestHandler):

    def get(self):
        #Main html file will need to be render here
        self.write("Hello World")
        # self.render("index.html")


def main():
    parse_command_line()
    app = application = tornado.web.Application([

        (r'/', IndexHandler),
        (r'/index', IndexHandler),

    ],**settings)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()